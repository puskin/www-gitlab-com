---
layout: markdown_page
title: "Scott Williamson's README"
---

## Scott W Readme

Tips for working & communicating with me:
* I am a synthesizer, and I often need time to absorb new information and form an opinion on what action to take.  As a result I tend not to think out loud in group settings.  I often wait to speak in group settings until my thoughts are clear and I believe I can contribute positively to the discussion. 
* I tend to think big picture.  One of my strengths is systems thinking, and being able to understand the interconnections between different things.  However, remembering specific numbers, dates, or technical details sometimes eludes me.
* I believe the strength of your relationships is a huge part of a Product leader’s success.  It’s all about the people, and people do their best work when they feel trust on a daily basis, and trust comes from shared experience.  As a result, I’m most willing to invest in building healthy relationships with my team and peers, so don’t be shy about asking for time with me.  
* I believe in servant leadership.  A leader’s job is to serve his or her team by providing clarity, removing obstacles, and coaching.  Hold me accountable if I’m not doing these things.
* I tend not to get too high or too low.  This has largely been a positive for me, as maintaining calm in the face of the stresses and strains of high growth can be stabilizing for the team.  The downside is that I may not exude passion and excitement.  But trust me, the fire is lit, it’s just a steady burn.
* I believe in the power of real time candid feedback.  I will endeavor to give real time candid feedback, and will appreciate real time candid feedback from others on my own performance as well.  
That said, I do not relish conflict, and can sometimes be slow to challenge others.  I’m working on this.
* If I commit to something, I will get it done.  And I expect the same from others.  Let’s hold each other accountable.
* Time is precious.  For meetings with me, always have an agenda and moderate the discussion proactively.  And hold me accountable if I’m wasting your time.
* I use email as a task list, so if you’d like me to take an action, email is a good communication channel.
* I like to focus on one thing at a time, so I turn off notifications for email and Slack.  Please expect my responses on email and Slack to be asynchronous and I’ll respond when I have time.
* If something is urgent, feel free to call or text me.
* I prefer to keep meetings between 8:30am and 5:30pm Mountain time.  My early mornings are for workouts and my evenings are for family.  I will make exceptions when necessary.
