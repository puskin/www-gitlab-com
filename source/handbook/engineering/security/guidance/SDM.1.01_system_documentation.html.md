---
layout: handbook-page-toc
title: "SDM.1.01 - System Documentation Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SDM.1.01 - System Documentation

## Control Statement

Documentation of system boundaries and key aspects of their functionality are published to authorized personnel. 

## Context

This control formalizes the idea that we need to keep track of our production systems and maintain quality documentation for easy reference. Most of this control is naturally met by the emphasis we have on documentation, here at GitLab. The remainder of this control is meant to ensure we are publishing any institutional knowledge about how systems interact and that we consider high-level system views as well as individual components. Testing of this control is to see if GitLab has comprehensive document information about our systems and system components. We easily meet this requirement given the nature of our handbook and GitLab's transparency.

## Scope

This control applies to all GitLab production systems.

## Ownership

* Control owner: Compliance team
* Process owner: N/A - All GitLabbers contribute to the handbook and constantly maintain documentation of GitLab systems and system components.

## Guidance

The most common form of system documentation is network and data flow diagrams.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [System Documentation control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/873).

### Policy Reference
1.Design & architecture
* https://docs.gitlab.com/ee/development/architecture.html
* https://docs.gitlab.com/ee/user/gitlab_com/architecture.html

2.Monitoring/Performance
*  https://docs.gitlab.com/ee/administration/monitoring/performance/index.html

3.Engineering
*  https://about.gitlab.com/handbook/#engineering
*  https://about.gitlab.com/handbook/engineering/performance/

4.Infrastructure
* https://about.gitlab.com/handbook/engineering/infrastructure/
* https://about.gitlab.com/handbook/engineering/infrastructure/production-architecture/

5.Production/Test/Development
* https://about.gitlab.com/handbook/engineering/development/
* https://about.gitlab.com/handbook/engineering/quality/

6.Back-up/recovery
* https://about.gitlab.com/handbook/engineering/infrastructure/production/#backups
* https://about.gitlab.com/handbook/engineering/infrastructure/database/#backup-and-recovery

7.Security
* https://about.gitlab.com/handbook/security/
* https://about.gitlab.com/handbook/engineering/security/#information-security-policies

8.Compliance
* https://about.gitlab.com/handbook/legal/global-compliance/
* https://about.gitlab.com/handbook/engineering/security/#compliance

## Framework Mapping

* SOC2 CC
  * CC2.3
