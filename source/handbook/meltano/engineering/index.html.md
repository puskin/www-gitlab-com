---
layout: handbook-page-toc
title: "Meltano Engineering Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Code

See the active project here: [gitlab.com/meltano/meltano](https://gitlab.com/meltano/meltano)

Read our documentation here: [https://meltano.com/docs/](https://meltano.com/docs/)

## Triage process

The `flow::Triage` label is used on issues that need product/prioritization triage by the Product Manager (Danielle), or engineering/assignment triage by the Engineering Lead (Douwe).
After they've been triaged, they'll have a milestone (other than `Backlog`), an assignee, and the `flow::To Do` label.

If you come across something that needs fixing:

1. Create an issue describing the problem.
2. If it's not obvious, justify how it relates to our persona and how it contributes to MAUI.
3. Then:

    - If it's more urgent (has a higher impact on MAUI) than other things you've been assigned, assign it to yourself to work on later the same week:

      ```md
      /milestone %<current milestone>
      /label ~"flow::To Do"
      /reassign @<yourself>
      /cc @DouweM
      ```

    - If it's urgent, but you're not sure who should work on it, assign it to Douwe to triage:

      ```md
      /milestone %<current milestone>
      /label ~"flow::Triage"
      /reassign @DouweM
      ```

    - If it's _not_ urgent or you're unsure whether it's something we should do at all, assign it to Danielle to triage:

      ```md
      /milestone %“Backlog" or %<next milestone>
      /label ~"flow::Triage"
      /reassign @dmor
      ```

## Useful issue boards

- [Development Flow](https://gitlab.com/groups/meltano/-/boards/536761), with a column for each `flow::` label. Don't forget to filter by milestone, and/or assignee!
- [Team Assignments](https://gitlab.com/groups/meltano/-/boards/1402405), with a column for each team member. Don't forget to filter by milestone!
- [Current Milestone](https://gitlab.com/groups/meltano/-/boards/1288307), with a column for each `flow::` label _and_ each team member.
- [Next Milestone](https://gitlab.com/groups/meltano/-/boards/1158410), with a column for each `flow::` label _and_ each team member.

## Release Process

### Schedule

Every Monday, we do a minor release (1.x) that is accompanied by a blog post and speedrun video.

Additionally, we do a patch (1.x.y) or minor release every subsequent day of the work week, to get improvements out to users as soon as they're ready.

#### Main releases

The main releases on Monday are owned by engineers on a rotating schedule:

| Release Date | Release Owner |
| ------------ | ------------- |
| 2019-12-16   | Ben           |
| 2019-12-23   | Yannis        |
| 2019-12-30   | Derek         |
| 2020-01-06   | Micael        |
| 2020-01-13   | Ben           |
| 2020-01-20   | Yannis        |
| 2020-01-27   | Derek         |

If you are unable to cover an assigned week, please find someone to cover for you and submit an MR to this page with the new owner.

#### Daily patch releases

The daily patch releases are currently owned by Yannis, so that they can be done during the European morning, in time for the first sales call in the American morning.

### Versioning

Meltano uses [semver](https://semver.org/) as its version number scheme.

### Prerequisites

Ensure you have the latest `master` branch locally before continuing.

```bash
git fetch origin
```

### Workflow

Meltano uses tags to create its artifacts. Pushing a new tag to the repository will publish it as docker images and a PyPI package.

1. Meltano has a number of dependencies for the release toolchain that are required when performing a release. If you haven't already, please navigate to your meltano installation and run the following command to install all development dependencies:

   ```bash
   # activate your virtualenv
   source ./venv/bin/activate

   # pip3 install all the development dependencies
   pip3 install .[dev]
   ```

2. Execute the commands below:

   ```bash
   # create and checkout the `release-next` branch from `origin/master`
   git checkout -B release-next origin/master

   # view changelog (verify changes made match changes logged)
   changelog view

   # after the changelog has been validated, tag the release
   make release

   # ensure the tag once the tag has been created, check the version we just bumped to: e.g. `0.22.0` => `0.23.0`.
   git describe --tags --abbrev=0

   # push the tag upstream to trigger the release pipeline
   git push origin $(git describe --tags --abbrev=0)

   # push the release branch to merge the new version, then create a merge request
   git push origin release-next
   ```

**Tip:** Releasing a hotfix? You can use `make type=patch release` to force a patch release. This is useful when we need to release hotfixes.

1. Create a merge request from `release-next` targeting `master` and use the `release` template.
2. Add the pipeline link (the one that does the actual deployment) to the merge request. Go to the commit's pipelines tab and select the one that has the **publish** stage.
3. Make sure to check `delete the source branch when the changes are merged`.
4. When the **publish** pipeline succeeds, the release is publicly available on [PyPI](https://pypi.org/project/meltano/).
5. Follow the [Digital Ocean publish process](#digitalocean-marketplace)
6. Upgrade all MeltanoData.com instances by running the [`meltano-upgrade.yml` Ansible playbook](https://meltano.com/handbook/engineering/meltanodata-guide/controller-node.html#meltano-upgrade-yml)
7. If a non-patch release, record and distribute the [Speedrun Video](#speedruns):
   - Meltano YouTube channel using the [Meltano YouTube guidelines](/handbook/meltano/marketing/#youtube)
   - Meltano Slack `@channel` in `#general`

## Demo Day

For each demo day, we need to ensure that the following process is followed:

### Demo Day: Setup

1. Document list of features to demo
2. Document order of people demoing
3. Ensure every person demoing has proper display size (i.e., font sizes, zoomed in enough, etc.)
   - Font size at least 20px
   - Browser zoom at least 125%

### Demo Day: Workflow

1. Record each meeting with Zoom
2. Generate list of timestamps for each featured demo
3. Generate list of features (from Setup section) paired with timestamps
4. Upload recording to YouTube
5. Add features + timestamps to YouTube description

## Speedruns

As part of Meltano's [Release process](#release-process), speedruns allow the team to ensure that every release is stable.

### 🏆 Current Record: 1:35 (Ben Hong)

**Tip:** Remember to leave each screen up for at least 2 seconds so users have a chance to notice that something actually happened.

### Requirements

1. Keep keystrokes to a minimum (ideally zero)
2. You do not have to explain every step
3. Do not need to stop and explain new features
4. Time starts from when the webapp is loaded on the browser
5. Make sure to pause between screens so user has a chance to register that a change is happening

### Workflow

1. Introduce Meltano to new users:

```md
Meltano is an open source data toolkit
that makes it easy to go from data source to dashboard.

For more information, check us out at meltano.com!
```

2. Check that Meltano does not exist on your machine

```bash
meltano --version
# command not found: meltano
```

3. Install Meltano on your machine using distributed version

```bash
pip3 install meltano
```

4. Check Meltano version matches latest release

```bash
meltano --version
```

5. Create a new Meltano project

```bash
meltano init speedrun-workflow
```

6. Change directory into your new project

```bash
cd speedrun-workflow
```

7. Start Meltano application

```bash
meltano ui
```

8. Assuming there are no conflicts on the port, you can now open your Meltano instance at <http://localhost:5000>.

9. Run through `tap-gitlab` + `target-postgres` workflow as quickly as possible with some narration, but don't pause mid-action to explain something.

## DigitalOcean Marketplace

Meltano is deployed and available as a [DigitalOcean Marketplace 1-Click install](https://marketplace.digitalocean.com/apps/meltano?action=deploy&refcode=1c4623f89322).

### Find the snapshot name

**Tip:** The _digitalocean_marketplace_ job is only available on pipelines running off `tags`.

1. The snapshot string should be available under `meltano-<timestamp>` on DigitalOcean, which you will find at the bottom of the _digitalocean_marketplace_ job. Take note of this snapshot string as you'll use it in the next step.

### Update the DigitalOcean listing

Then, head to the DigitalOcean vendor portal at <https://marketplace.digitalocean.com/vendorportal> to edit the Meltano listing.

**Tip:** Don't see the Meltano listing? You'll have to be granted access to the DigitalOcean vendor portal. Please ask your manager for access.

1. Once inside the listing, update the following entries:
   - **Version** to the latest Meltano version
   - **System Image** to the new image (match the aforementioned snapshot string)
   - **Meltano Package Version** inside the _Software Included Entry_
2. Submit it for review to finish the process.
